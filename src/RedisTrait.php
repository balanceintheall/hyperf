<?php
declare(strict_types=1);

namespace Reven\HyperfRedisBasics;;

use Hyperf\Redis\Redis;
use Hyperf\Utils\ApplicationContext;

Trait RedisTrait
{

    protected  Redis $redis;

    public function init(){
        $this->redis=ApplicationContext::getContainer()->get(Redis::class);
    }
}