<?php
declare(strict_types=1);

namespace Reven\HyperfRedisBasics\exception;

use \Exception;

/**
 * 并发异常
 * Class ConcurrencyException
 * @package HyperfRedis\exception
 */
class ConcurrencyException extends Exception
{

}